package facci.jipsychica.jipsyapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private EditText text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (EditText) findViewById(R.id.editText1);
        Log.d(TAG, "Jipsy Chica");

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.calcButton:
                RadioButton celsiusButton = (RadioButton) findViewById(R.id.celsius);
                RadioButton fahrenheitButton = (RadioButton) findViewById(R.id.farenheit);
                TextView output = (TextView) findViewById(R.id.textView1);
                if (text.getText().length() == 0) {
                    Toast.makeText(this, "Por favor ingrese un numero valido",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                float inputValue = Float.parseFloat(text.getText().toString());
                if (celsiusButton.isChecked()) {
                    String result = String.valueOf(Conversor.convertirCelsiusAFahrenheit(inputValue));
                    output.setText("= " + result + " farenheit");
                } else {
                    String result = String.valueOf(Conversor.convertirFahrenheitACelsius(inputValue));
                    output.setText("= " + result + " celsius");
                }
                break;
        }
    }
}
