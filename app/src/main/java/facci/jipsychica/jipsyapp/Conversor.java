package facci.jipsychica.jipsyapp;

public class Conversor {

    public static float convertirFahrenheitACelsius(float fahrenheit) {
        return ((fahrenheit - 32) * 5 / 9);
    }


    public static float convertirCelsiusAFahrenheit(float celsius) {
        return ((celsius * 9) / 5) + 32;
    }
}
